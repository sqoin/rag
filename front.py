
HTML_TEMPLATE = """
<!DOCTYPE html>
<html>
<head>
    <title>Retrieval Augmentation Generation Test</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            text-align: center;
            padding: 50px;
        }
        h2 {
            color: #333;
        }
        form {
            margin: 20px auto;
            width: 300px;
            max-width: 300px; /* Limiting the maximum width */
            padding: 20px 20px;
            background: white;
            border-radius: 10px;
            box-shadow: 2px 2px 12px rgba(0,0,0,0.1);
        }
        label, input, button {
            width: calc(100% - 20px); /* Adjust for padding */
            display: block;
            margin-bottom: 20px;
        }
        input, button {
            width: calc(100% - 20px); /* Adjust for padding */
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }
        button {
            background-color: #5cb85c;
            color: white;
            border: none;
        }
        button:hover {
            background-color: #4cae4c;
        }
        .response {
            margin-top: 20px;
            background: white;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 2px 2px 12px rgba(0,0,0,0.1);
        }
    </style>
</head>
<body>
    <h2>Retrieval Augmentation Generation Test</h2>
    <form method="POST">
        <label for="question">Enter your question:</label>
        <center> <input type="text" id="question" name="question"> </center>
        <center> <button type="submit">Submit</button> </center>
    </form>
    <br><br>
    {% if question %}
        <h3>Prompt</h2>
        <div class="response">
            {{ question }}
        </div>
    {% endif %}
    <br><br>
    <h3>Response</h2>
    {% if response %}
        <div class="response">
            {{ response }}
        </div>
    {% endif %}
</body>
</html>
"""
