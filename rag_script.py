# Import transformer classes for generaiton
from transformers import AutoTokenizer, AutoModelForCausalLM, TextStreamer
# Import torch for datatype attributes
import torch
# Import the prompt wrapper...but for llama index
from llama_index.prompts.prompts import SimpleInputPrompt
# Import the llama index HF Wrapper
from llama_index.llms import HuggingFaceLLM
# Bring in embeddings wrapper
from llama_index.embeddings import LangchainEmbedding
# Bring in HF embeddings - need these to represent document chunks
from langchain.embeddings.huggingface import HuggingFaceEmbeddings
# Bring in stuff to change service context
from llama_index import set_global_service_context
from llama_index import ServiceContext
# Import deps to load documents
from llama_index import VectorStoreIndex, download_loader
from pathlib import Path

if torch.cuda.is_available():
    torch.cuda.empty_cache()

# Define variable to hold llama2 weights naming 
name = "NousResearch/Llama-2-7b-chat-hf"
# Set auth token variable from hugging face 
auth_token = "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"
# Create tokenizer
tokenizer = AutoTokenizer.from_pretrained(name, token=auth_token)


# Create model
model = AutoModelForCausalLM.from_pretrained(
    name, 
    use_auth_token=auth_token, 
    torch_dtype=torch.float16, 
#     load_in_8bit=True,
    load_in_4bit=True
#     rope_scaling={"type": "dynamic", "factor": 2}, 
    
) 
     
    
# Setup a prompt 
prompt = "### User:What is the fastest car in  \
          the world and how much does it cost? \
          ### Assistant:"
# Pass the prompt to the tokenizer
#inputs = tokenizer(prompt, return_tensors="pt").to(model.device)
# Setup the text streamer 
#streamer = TextStreamer(tokenizer, skip_prompt=True, skip_special_tokens=True)

# Create a system prompt 
system_prompt = """[INST] <>
You are a helpful, respectful assistant in an Hotel called Paradise Ranch. Always answer only precise answers, while being safe. Your answers should not include
any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content.
Please ensure that your responses are socially unbiased and positive in nature.

If a question does not make any sense, or is not factually coherent, explain 
why instead of answering something not correct. If you don't know the answer 
to a question, please don't share false information.

Your goal is to provide answers relating to the Paradise Ranch Hotel.<>
"""
# Throw together the query wrapper
query_wrapper_prompt = SimpleInputPrompt("{query_str} [/INST]")

# Complete the query prompt
query_wrapper_prompt.format(query_str='hello')

# Create a HF LLM using the llama index wrapper
llm = HuggingFaceLLM(context_window=4096,
                    max_new_tokens=256,
                    system_prompt=system_prompt,
                    query_wrapper_prompt=query_wrapper_prompt,
                    model=model,
                    tokenizer=tokenizer)

# Bring in embeddings wrapper
from llama_index.embeddings import LangchainEmbedding
# Bring in HF embeddings - need these to represent document chunks
from langchain.embeddings.huggingface import HuggingFaceEmbeddings

# Create and dl embeddings instance
embeddings=LangchainEmbedding(
    HuggingFaceEmbeddings(model_name="all-MiniLM-L6-v2")
)

# Create new service context instance
service_context = ServiceContext.from_defaults(
    chunk_size=1024,
    llm=llm,
    embed_model=embeddings
)
# And set the service context
set_global_service_context(service_context)


# Download PDF Loader
PyMuPDFReader = download_loader("PyMuPDFReader")
# Create PDF Loader
loader = PyMuPDFReader()

# Convert Path to string
file_path_str = str(Path('./data/dataset.pdf'))

# Load documents
documents = loader.load(file_path=file_path_str, metadata=True)

print("\n\n===========\n  Data Loaded  \n===========\n\n")

# Create an index - we'll be able to query this in a sec
index = VectorStoreIndex.from_documents(documents)

print("\n\n===========\n  index set up  \n===========\n\n")


# Setup index query engine using LLM
query_engine = index.as_query_engine()

print("\n\n===========\n  Engine set up  \n===========\n\n")

def test(question) :

    #question = input("\n\nEnter your question : ")
    print("\n\n")

    # Test out a query in natural
    response = query_engine.query(question)

    print("\n\n===========\n  Testing  \n===========\n\n")

    print(f"\n\n =============================== Output ====================================================\n")
    print(f"Question : {question}")
    print(f"\n\nAnswer : {response}")
    print(f"\n\n ===========================================================================================\n\n")
