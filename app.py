from flask import Flask, request, render_template_string
from flask_cors import CORS
import rag_script
import front

app = Flask(__name__)
CORS(app)

def test_model(question):
    return rag_script.query_engine.query(question)


@app.route('/', methods=['GET', 'POST'])
def index():
    response = None

    if request.method == 'POST':
        question = request.form['question']
    if request.method == "GET" :
         question = "Hello"

    # Call the query engine from your script
    response = test_model(question)
    return render_template_string(front.HTML_TEMPLATE, response=response, question=question)

if __name__ == '__main__':
#    app.run(debug=True)
    app.run(host='0.0.0.0', debug=False, port=5000)
